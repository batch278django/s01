from django.shortcuts import render

# Create your views here.


# from keyword allows importing of necessary classes/ modules, methos and other files needed in our application from the django.http
from django.http import HttpResponse


# local imports

from .models import GroceryItem


from django.contrib.auth.models import User

#import the auth function

from django.contrib.auth import authenticate, login, logout

from django.forms.models import model_to_dict


def index(request):
	grocery_list = GroceryItem.objects.all()
	# template = loader.get_template("index.html")
	context = {
		'grocery_list' : grocery_list,
		"user": request.user
	}
	return render(request, "index.html", context)



def groceryitem(request, grocery_item):
	groceryitem = model_to_dict(GroceryItem.objects.get(pk = grocery_item))

	return render(request, "groceryitem.html", groceryitem)


def register(request):

	users = User.objects.all()
	is_user_registered = False

	user = User()
	user.username = "johndoe"
	user.first_name = "john"
	user.last_name = "doe"
	user.email = "johndoe@gmail.com"
	user.set_password("john123")
	user.is_staff = False
	user.is_active - True

	for indiv_user in users:
		if indiv_user.username == user.username:
			is_user_registered = True
			break


	if is_user_registered == False:
		# to save our user
		user.save()

	context = {
		'is_user_registered' : is_user_registered,
		'first_name' : user.first_name,
		'last_name' : user.last_name
	}

	return render(request, "register.html", context)



def change_password(request):

	is_user_auth = False

	user = authenticate(username = "johndoe", password = "john123")


	if user is not None:
		authenticated_user = User.objects.get(username = 'johndoe')
		authenticated_user.set_password("john1234")
		authenticated_user.save()

		is_user_auth = True


	context = {
		"is_user_auth" : is_user_auth
	}


	return render(request, "change_password.html", context)




def login_user(request):
	username = "johndoe"
	password = 'john1234'

	

	user = authenticate(username = username, password = password)

	if user is not None:
		login(request, user)
		return redirect(index)

	else:
		return render(request, "login.html")


def logout_user(request):
	logout(request)
	return redirect("index")