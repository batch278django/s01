

from django.urls import path

#import the views.py in the same folder
from . import views

urlpatterns = [
	path('index', views.index, name = 'index'),
	path('<int:grocery_item>', views.groceryitem, name = 'viewgrocery'),
	path('register', views.register, name = 'register'),
	path('changepassword', views.change_password, name = "changepassword"),
	path('login', views.login_user, name = "login"),
	path('logout', views.logout_user, name = "logout")
]