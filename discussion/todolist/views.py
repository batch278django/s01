from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.


# from keyword allows importing of necessary classes/ modules, methos and other files needed in our application from the django.http
from django.http import HttpResponse

# local imports

from .models import ToDoItem, Addevent


from django.contrib.auth.models import User

#import the auth function

from django.contrib.auth import authenticate, login, logout

from django.forms.models import model_to_dict

from .forms import LoginForm, AddTaskForm, UpdateTaskForm, AddEventForm, UpdateEventForm

from django.utils import timezone

# to use templates created

# from django.template import loader


def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
	eventitem_list = Addevent.objects.filter(user_id=request.user.id)
	# template = loader.get_template("index.html")
	context = {
		'todoitem_list' : todoitem_list,
		'eventitem_list' : eventitem_list,
		"user": request.user
	}
	# output = ", ".join([todoitem.task_name for todoitem in todoitem_list])


	# render(request, route_template, context)
	return render(request, "index.html", context)


	# return HttpResponse("Hello from the views.py file!")



def todoitem(request, todoitem_id):
	# response = f"You are viewing the details of {todoitem_id}"
	# return HttpResponse(response)

	todoitem = get_object_or_404(ToDoItem, pk = todoitem_id)

	return render(request, "todoitem.html", model_to_dict(todoitem))


# this function is responsible for registering on our application

def register(request):
    if request.method == 'POST':
 
        username = request.POST.get('username')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        password = request.POST.get('password')
        confirm_password = request.POST.get('confirm_password')
        
  
        if User.objects.filter(username=username).exists():
            context = {'is_user_registered': True}
            return render(request, 'register.html', context)

   
        if password != confirm_password:
            context = {'passwords_mismatch': True}
            return render(request, 'register.html', context)


        user = User.objects.create_user(username=username, email=email, password=password, first_name=first_name, last_name=last_name)


        user = authenticate(username=username, password=password)
        login(request, user)
        
        return redirect('todolist:index')
        
    else:
        return render(request, 'register.html')





def change_password(request):

	is_user_auth = False

	user = authenticate(username = "johndoe", password = "john123")


	if user is not None:
		authenticated_user = User.objects.get(username = 'johndoe')
		authenticated_user.set_password("john1234")
		authenticated_user.save()

		is_user_auth = True


	context = {
		"is_user_auth" : is_user_auth
	}


	return render(request, "change_password.html", context)




def login_user(request):
	context = {

	}

	# if this is a post request we need to process the form data
	if request.method == "POST":

		form = LoginForm(request.POST)
		if form.is_valid() == False:
			form = LoginForm()
			print(form)
		else:
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(username = username, password = password)

			if user is not None:
				context = {
					'username' : username,
					'password' : password
				}

				login(request, user)


				return redirect('todolist:index')
			else:
				context = {
					'error' : True
				}	

		
	return render(request, "login.html", context)

	# username = "johndoe"
	# password = 'john1234'

	

	# user = authenticate(username = username, password = password)




def logout_user(request):
	logout(request)
	return redirect("todolist:index")

def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name = task_name, user_id = request.user.id)

			if not duplicates:
				# create an object based on the ToDoItem model and saves to the record in the database

				ToDoItem.objects.create(task_name = task_name, description = description, date_created = timezone.now(), user_id = request.user.id)
				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}
	

	return render(request, "add_task.html", context)




def update_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk = todoitem_id)

	context= {
		"user" : request.user,
		"todoitem_id" : todoitem_id,
		"task_name" : todoitem[0].task_name,
		"description" : todoitem[0].description,
		"status" : todoitem[0].status
	}

	if request.method == "POST":
		form = UpdateTaskForm(request.POST)

		if form.is_valid() == False:
			form = UpdateTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']


			if todoitem:

				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status


				todoitem[0].save();

				return redirect("todolist:viewtodoitem" , todoitem_id = todoitem[0].id)
			else:

				context = {
					"error" : True
				}


	return render(request, "update_task.html", context)




def delete_task(request, todoitem_id):

	ToDoItem.objects.filter(pk = todoitem_id).delete()


	return redirect("todolist:index")





def add_event(request):
	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			duplicates = Addevent.objects.filter(event_name = event_name, user_id = request.user.id)

			if not duplicates:
				

				Addevent.objects.create(event_name = event_name, description = description, date_created = timezone.now(), user_id = request.user.id)
				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}
	

	return render(request, "add_event.html", context)


def eventitem(request, eventitem_id):
    eventitem = get_object_or_404(Addevent, pk=eventitem_id)
    return render(request, "eventitem.html", model_to_dict(eventitem))



def update_event(request, eventitem_id):

	eventitem = Addevent.objects.filter(pk = eventitem_id)

	context= {
		"user" : request.user,
		"eventitem_id" : eventitem_id,
		"event_name" : eventitem[0].event_name,
		"description" : eventitem[0].description,
		"status" : eventitem[0].status
	}

	if request.method == "POST":
		form = UpdateEventForm(request.POST)

		if form.is_valid() == False:
			form = UpdateEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']


			if eventitem:

				eventitem[0].event_name = event_name
				eventitem[0].description = description
				eventitem[0].status = status


				eventitem[0].save();

				return redirect("todolist:vieweventitem" , eventitem_id = eventitem[0].id)
			else:

				context = {
					"error" : True
				}


	return render(request, "update_event.html", context)
