from django import forms
from django.contrib.auth.models import User


class LoginForm(forms.Form):
	username = forms.CharField(label = "username", max_length = 20)
	password = forms.CharField(label = "password", max_length = 20)


class AddTaskForm(forms.Form):
	task_name = forms.CharField(label = "task_name", max_length = 50)
	description = forms.CharField(label = "description", max_length = 50)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label = "task_name", max_length = 50)
	description = forms.CharField(label = "description", max_length = 50)
	status = forms.CharField(label = "status", max_length = 50)


class UpdateEventForm(forms.Form):
	event_name = forms.CharField(label = "event_name", max_length = 50)
	description = forms.CharField(label = "description", max_length = 50)
	status = forms.CharField(label = "status", max_length = 50)


class AddEventForm(forms.Form):
	event_name = forms.CharField(label = "event_name", max_length = 50)
	description = forms.CharField(label = "description", max_length = 50)




class RegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password', 'confirm_password']

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password and confirm_password and password != confirm_password:
            raise ValidationError("Passwords do not match")

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

