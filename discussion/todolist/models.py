from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class ToDoItem(models.Model):
	task_name = models.CharField(max_length=50)
	description = models.CharField(max_length=200)
	status = models.CharField(max_length=50, default="pending")
	date_created = models.DateTimeField('date_created')
	user = models.ForeignKey(User, on_delete = models.RESTRICT, default = "")




class Addevent(models.Model):
    event_name = models.CharField(max_length=200)
    description = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete= models.RESTRICT, default = "")




class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
