# [Section] Models
# Each model is represented by a class that inherits the django.db.models.Model. Each model has a number of class variable, each of which represents a database field in the model

# Each fiel is represented by an instance of a field class e.g. charfield for character fields and DateTimeField for date times. this tells django what type of data each field holds.

# Some field classes have required arguments. Charfield requires that you give it a max_length.

# A field can also have various optional arguments. in this case, we've set the default value of status to pending



# Migration



# SHELL


todoitem = ToDoItem(task_name = "Eat", description = "Dinner", date_created = timezone.now())



# save

todoitem.save()


# creating SUPERUSER
# windows
winpty python manage.py createsuperuser